# CUSTOM HOTKEYS
# volume
XF86AudioRaiseVolume
	pamixer -i 5

XF86AudioLowerVolume
	pamixer -d 5

XF86AudioMute
	pamixer -t 5

# brightness
XF86MonBrightnessUp
	brightnessctl set +5%

XF86MonBrightnessDown
	brightnessctl set 5%-

# terminal
super + Return
  alacritty

# rofi drun
super + r
	rofi -show drun

# rofi run
super + shift r
	rofi -show run

# rofi emoji
super + comma
	rofi -show emoji

# enable redshift
super + XF86MonBrightnessUp
	redshift LAT:LON

# disable redshift
super + XF86MonBrightnessDown
	killall -q redshift

# file manager
super + f
	pcmanfm

# web browser
super + b
	firefox

# visual stuidio code
super + v
	code

# neovim
super + n
  alacritty -e nvim

# screenshot
Print
	sh $HOME/.config/bspwm/scripts/scrot.sh

super + Print
	sh $HOME/.config/bspwm/scripts/scrot_select.sh

super + alt + Print
	sh $HOME/.config/bspwm/scripts/scrot_window.sh

# betterlockscreen
super + Escape
	betterlockscreen --lock dimblur

# WM INDEPENDENT KEYBINDINGS
# make sxhkd reload its configuration files:
alt + Escape
	pkill -USR1 -x sxhkd && notify-send "sxhkd restarted"

# BSPWM HOTKEYS
# quit/restart bspwm
super + alt + r
	bspc {wm -r} && notify-send "bspwm restarted"

super + alt + q
	bspc quit

# close and kill
super + {_,shift + }w
	bspc node -{c,k}

# alternate between the tiled and monocle layout
alt + m
	bspc desktop -l next

# Focused desktop window gap
super + alt + {Down,Up}
	bspc config -d focused window_gap $((`bspc config -d focused window_gap` {-,+} 5 ))

# STATE/FLAGS
# set the window state
alt + {t,f,shift + f}
	bspc node -t {tiled,floating,fullscreen}

alt + g
	bspc node -g sticky

# FOCUS/SWAP
# move to desktop
super + {1-5}
	bspc desktop -f '^{1-5}'

# focus or send to the given desktop
super + {_,shift + }{1-5}
	bspc {desktop -f,node -d} '^{1-5}'

# focus the next/previous node in the current desktop
alt + Tab
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + Tab
	bspc desktop -f {prev,next}.local

# MOVE/RESIZE
# expand a window by moving one of its side outward
ctrl + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
alt + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
ctrl + alt + {Left,Down,Up,Right}
	bspc node -v {-5 0,0 5,0 -5,5 0}
