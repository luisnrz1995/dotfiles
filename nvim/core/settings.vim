syntax on

set background=dark
set termguicolors

set number
set relativenumber

set cursorcolumn
set cursorline

set autochdir

set autoindent
set expandtab
set smartindent
set smarttab
set shiftwidth=2
set tabstop=2

set nobackup
set noswapfile
set nowritebackup

set hlsearch
set ignorecase
set incsearch
set smartcase

set encoding=utf-8
set linebreak
set scrolloff=1
set signcolumn=yes
set nowrap

set laststatus=2
set noshowmode
set ruler
set showcmd
set title
set wildmenu

set confirm
set hidden

set updatetime=300

set completeopt=longest,menuone
set backspace=indent,eol,start

set cmdheight=1

set splitbelow
set splitright
