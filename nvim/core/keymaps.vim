" Set space as the leader key.
let mapleader = ' '

" Reload init.vim
nnoremap <leader>R :source $HOME/.config/nvim/init.vim<CR>

" Save and quit.
nnoremap <C-s> :w<CR>
nnoremap <C-w> :wq<CR>
nnoremap <C-q> :q<CR>

" Delete characters without copiyng them.
nnoremap x "_x
nnoremap d "_d
nnoremap D "_D
vnoremap d "_d

" File explorer.
noremap <C-n> :Neotree<CR>

" Manage buffers.
noremap <A-,> :bprevious<CR>
noremap <A-.> :bnext<CR>
noremap <A-x> :bd<CR>

" Split window.
nnoremap <leader>v <C-w>v
nnoremap <leader>h <C-w>s
nnoremap <leader>x <C-w>o

" Moving between splits.
noremap <C-left> :wincmd h<CR>
noremap <C-down> :wincmd j<CR>
noremap <C-up> :wincmd k<CR>
noremap <C-right> :wincmd l<CR>

" Resize splits.
noremap <S-left> :vertical resize +5<CR>
noremap <S-down> :horizontal resize +5<CR>
noremap <S-up> :horizontal resize -5<CR>
noremap <S-right> :vertical resize -5<CR>

" Open terminal.
nnoremap <silent><leader>th <Cmd>exe v:count1 . "ToggleTerm size=20 direction=horizontal"<CR>
nnoremap <silent><leader>tv <Cmd>exe v:count1 . "ToggleTerm size=75 direction=vertical"<CR>

" Format document.
nnoremap <leader>df :Format<CR>
