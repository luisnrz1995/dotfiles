" Plugins installed
call plug#begin()
  " Gruvbox material theme.
  Plug 'sainnhe/gruvbox-material'

  " Conquer of completion
  Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

  " Status line
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

  " Icons
  Plug 'ryanoasis/vim-devicons'

  " Intentation marks.
  Plug 'lukas-reineke/indent-blankline.nvim'

  " File explorer
  Plug 'nvim-neo-tree/neo-tree.nvim'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'kyazdani42/nvim-web-devicons'
  Plug 'MunifTanjim/nui.nvim'

  " Temrinal emulator
  Plug 'akinsho/toggleterm.nvim', {'tag' : '*'}

  " Syntax highlighting
  Plug 'nvim-treesitter/nvim-treesitter'

  " Auto close HTML/XML tags.
  Plug 'alvan/vim-closetag'

  " Make comments easier
  Plug 'tpope/vim-commentary'

  " Delete/change/add parentheses/quotes/XML-tags/much more with ease.
  Plug 'tpope/vim-surround'

  " Unofficial plugin for Vim/Nvim to use Live Server functionalities.
  Plug 'manzeloth/live-server'

  " The Neovim fastest colorizer.
  Plug 'norcalli/nvim-colorizer.lua'

  Plug 'lewis6991/impatient.nvim'
call plug#end()


" Import plugins configurations.
source $HOME/.config/nvim/plugins/configs/gruvbox-material.vim
source $HOME/.config/nvim/plugins/configs/coc.vim
source $HOME/.config/nvim/plugins/configs/vim-airline.vim
source $HOME/.config/nvim/plugins/configs/indent-blankline.vim
source $HOME/.config/nvim/plugins/configs/neo-tree.vim
source $HOME/.config/nvim/plugins/configs/toggleterm.vim
source $HOME/.config/nvim/plugins/configs/nvim-treesitter.vim
source $HOME/.config/nvim/plugins/configs/nvim-colorizer.vim
