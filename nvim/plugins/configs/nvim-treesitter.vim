lua << EOF
require'nvim-treesitter.configs'.setup {
   indent = {
    enable = true
  }
}
EOF
